<?php

$this->widget('ext.elFinder.ElFinderWidget', array(
        'connectorRoute' => 'elfinder/connector',
        'settings' => array(
            'getFileCallback' => 'js:function(file) { 
                var is_first = true;
                $("input[id$=_textInput]", window.opener.document).each(function()
                {
                    if (is_first) $(this).val(file.url);
                    is_first = false;
                });
                window.close();
             }'
          ),
        )
);

?>
