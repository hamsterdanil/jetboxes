<hr>
<footer class="row">
    <div class="span12">
        <ul class="inline copyright">
            <li>
                <?php echo CHtml::link(
                    Yii::t('default', 'Internet projects development and maintenance'),
                    'http://jetstyle.ru'
                ); ?>
            </li>
            <li class="pull-right">
                © 2013 - <?php echo date('Y'); ?> <?php echo CHtml::link(
                    'JetStyle',
                    'http://jetstyle.ru'
                ); ?>
            </li>
        </ul>
    </div>
</footer>
