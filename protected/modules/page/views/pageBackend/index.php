<?php
    $this->breadcrumbs = array(
        Yii::app()->getModule('page')->getCategory() => array(),
        Yii::t('PageModule.page', 'Pages') => array('/page/pageBackend/index'),
        Yii::t('PageModule.page', 'List'),
    );

    $this->pageTitle = Yii::t('PageModule.page', 'Pages list');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('PageModule.page', 'Pages list'), 'url' => array('/page/pageBackend/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('PageModule.page', 'Create page'), 'url' => array('/page/pageBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('PageModule.page', 'Pages'); ?>
        <small><?php echo Yii::t('PageModule.page', 'manage'); ?></small>
    </h1>
</div>

<?php $this->widget('yupe\widgets\CustomGridView', array(
    'id'           => 'page-grid',
    'type'         => 'condensed',
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'sortField'    => 'order',
    'columns'      => array(
        array(
            'name'  => 'id',
            'type'  => 'raw',
            'value' => 'CHtml::link($data->id, array("/page/pageBackend/update", "id" => $data->id))',
        ),
        array(
            'name'  => 'title',
            'type'  => 'raw',
            'value' => 'CHtml::link($data->title, array("/page/pageBackend/update", "id" => $data->id))',
        ),
        'title_short',
        array(
            'name'  => 'slug',
            'type'  => 'raw',
            'value' => 'CHtml::link($data->slug, array("/page/pageBackend/update", "id" => $data->id))',
        ),
        array(
            'name'  => 'category_id',
            'value' => '$data->getCategoryName()',
            'filter' => CHtml::listData($this->module->getCategoryList(),'id','name')
        ),
        array(
            'name'   => 'parent_id',
            'value'  => '$data->parentName',
            'filter' => CHtml::listData(Page::model()->findAll(),'id','title')
        ),

        array(
            'name'  => 'order',
            'type'  => 'raw',
            'value' => '$this->grid->getUpDownButtons($data)',
        ),
        array(
            'name'  => 'lang',
            'value'  => '$data->lang',
            'filter' => $this->yupe->getLanguagesList()
        ),
        array(
            'name'  => 'status',
            'type'  => 'raw',
            'value' => '$this->grid->returnBootstrapStatusHtml($data, "status", "Status", array("pencil", "ok-sign", "time"))',
            'filter' => $model->getStatusList()
        ),
        array(
            'header' => Yii::t('PageModule.page', 'Public URL'),
            'type'   => 'raw',
            'value'  => 'CHtml::link($data->getPermaLink(),$data->getPermaLink(),array("target" => "_blank"))',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>