<?php

$this->widget(
    'bootstrap.widgets.TbNavbar', array(
        'htmlOptions' => array('class' => 'navbar'),
        'fluid'       => true,
        'brand'       => CHtml::image(
            Yii::app()->baseUrl . "/web/images/logo.png", Yii::app()->name, array(
                'width'  => '38',
                'height' => '38',
                'title'  => Yii::app()->name,
            )
        ),
        'brandUrl'    => CHtml::normalizeUrl(array("/yupe/backend/index")),
        'items'       => array(
            array(
                'class' => 'bootstrap.widgets.TbMenu',
                'items' => $this->controller->yupe->getModules(true),
            ),
            array(
                'class'       => 'bootstrap.widgets.TbMenu',
                'htmlOptions' => array('class' => 'pull-right'),
                'encodeLabel' => false,
                'items'       => array_merge(
                    array(
                        array(
                            'icon'        => 'home white',
                            'label'       => Yii::t('YupeModule.yupe', 'Go home'),
                            'linkOptions' => array('target' => '_blank'),
                            'visible'     => Yii::app()->controller instanceof yupe\components\controllers\BackController === true,
                            'url'         => array('/' . Yii::app()->defaultController . '/index/'),
                        ),
                        array(
                            'label'       => '
                                <div style="float: left; line-height: 16px; text-align: center; margin-top: -10px;">
                                    <small style="font-size: 80%;">' . Yii::t('YupeModule.yupe', 'Administrator') . '</small><br />
                                    <span class="label">' . Yii::app()->user->nick_name . '</span>
                                </div>',
                            'encodeLabel' => false,
                            'items'       => array(
                                array(
                                    'icon'  => 'user',
                                    'label' => Yii::t('YupeModule.yupe', 'Profile'),
                                    'url'   => CHtml::normalizeUrl((array('/user/userBackend/update', 'id' => Yii::app()->user->getId()))),
                                ),
                                array(
                                    'icon'  => 'off',
                                    'label' => Yii::t('YupeModule.yupe', 'Exit'),
                                    'url'   => CHtml::normalizeUrl(array('/user/account/logout')),
                                ),
                            ),
                        ),
                    ), $this->controller->yupe->getLanguageSelectorArray()
                ),
            ),
        ),
    )
);