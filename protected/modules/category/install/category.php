<?php
/**
 * Конфигурационный файл модуля
 *
 * @author JetStyle <hamsterman@jetstyle.ru>
 * @link http://jetstyle.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.category.install
 * @since 0.1
 *
 */
return array(
    'module'   => array(
        'class' => 'application.modules.category.CategoryModule',
    ),
    'import'    => array(
        'application.modules.category.models.*',
    ),
    'component' => array(),
    'rules'     => array(),
);