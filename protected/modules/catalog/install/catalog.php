<?php
/**
 * Конфигурационный файл модуля
 *
 * @author JetStyle <hamsterman@jetstyle.ru>
 * @link http://jetstyle.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.catalog.install
 * @since 0.1
 *
 */
return array(
    'module'   => array(
        'class' => 'application.modules.catalog.CatalogModule',
    ),
    'import'    => array(
        'application.modules.catalog.models.*',
    ),
    'component' => array(),
    'rules'     => array(),
);