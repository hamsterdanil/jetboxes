<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo CHtml::encode(Yii::app()->name); ?> <?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php
    $mainAssets = Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('application.modules.yupe.views.assets')
    );
    Yii::app()->clientScript->registerCssFile($mainAssets . '/css/styles.css');
    Yii::app()->clientScript->registerScriptFile($mainAssets . '/js/main.js');
    Yii::app()->clientScript->registerScriptFile($mainAssets . '/js/jquery.li-translit.js');
    if (($langs = $this->yupe->languageSelectorArray) != array())
        Yii::app()->clientScript->registerCssFile($mainAssets. '/css/flags.css');
    ?>
    <link rel="shortcut icon" href="<?php echo $mainAssets; ?>/img/favicon.ico"/>

</head>

<body>
<div id="overall-wrap">
    <!-- mainmenu -->
    <?php  $brandTitle = Yii::t('UserModule.user', 'Control panel'); ?>
    <?php
            $this->widget(
                'bootstrap.widgets.TbNavbar', array(
                    'htmlOptions' => array('class' => 'navbar-inverse'),
                    'fluid'       => true,
                    'brand'       => CHtml::image(
                        Yii::app()->baseUrl . "/web/images/logo.png", Yii::app()->name, array(
                            'width'  => '38',
                            'height' => '38',
                            'title'  => Yii::app()->name,
                        )
                    ),
                    'brandUrl'    => CHtml::normalizeUrl(array("/yupe/backend/index")),
                    'items'       => array(
                        array(
                            'class'       => 'bootstrap.widgets.TbMenu',
                            'htmlOptions' => array('class' => 'pull-right'),
                            'encodeLabel' => false,
                            'items'       => array_merge(
                                array(
                                    array(
                                        'icon'        => 'home white',
                                        'label'       => Yii::t('UserModule.user', 'Go home'),
                                        'linkOptions' => array('target' => '_blank'),
                                        'url'         => array('/' . Yii::app()->defaultController . '/index/'),
                                    ),
                                ), $this->yupe->languageSelectorArray
                            ),
                        ),
                    ),
                )
            ); ?>
    <div class="container-fluid" id="page">
        <?php echo $content; ?>
        <div id="footer-guard"><!-- --></div>
    </div>
</div>

<footer>
    Copyright &copy; 2013-<?php echo date('Y'); ?>
    <?php //echo $this->yupe->poweredBy();?>
    <br/>
    <a href="http://jetstyle.ru">
        <?php echo Yii::t('UserModule.user', 'Development and support'); ?></a> - <a href="http://jetstyle.ru" target="_blank">Jetstyle
    </a>
</footer>

</body>
</html>