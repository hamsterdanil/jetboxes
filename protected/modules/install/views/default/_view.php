<?php
/**
 * Отображение для _view:
 * 
 *   @category YupeView
 *   @package  yupe
 *   @author   JetStyle <hamsterman@jetstyle.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->renderPartial(
    Yii::app()->controller->action->id, array(
        'data' => isset($data) ? $data : array(),
    )
);?>
