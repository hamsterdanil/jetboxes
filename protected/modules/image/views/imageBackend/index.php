<?php
    $this->breadcrumbs = array(
        Yii::app()->getModule('image')->getCategory() => array(),
        Yii::t('ImageModule.image', 'Images') => array('/image/imageBackend/index'),
        Yii::t('ImageModule.image', 'Management'),
    );

    $this->pageTitle = Yii::t('ImageModule.image', 'Images - manage');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('ImageModule.image', 'Image management'), 'url' => array('/image/imageBackend/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('ImageModule.image', 'Add image'), 'url' => array('/image/imageBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo ucfirst(Yii::t('ImageModule.image', 'Images')); ?>
        <small><?php echo Yii::t('ImageModule.image', 'management'); ?></small>
    </h1>
</div>

<?php
$this->widget(
    'yupe\widgets\CustomGridView', array(
        'id'           => 'image-grid',
        'type'         => 'condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => array(
            'id',
            array(
                'name'   => Yii::t('ImageModule.image', 'file'),
                'type'   => 'raw',
                'value'  => 'CHtml::image($data->getUrl(75), $data->alt, array("width" => 75, "height" => 75))',
                'filter' => false
            ),
            array(
                'name'   => 'category_id',
                'value'  => '$data->getCategoryName()',
                'filter' => CHtml::listData(Yii::app()->getModule('image')->getCategoryList(),'id','name')
            ),
            array(
                'name'   => 'galleryId',
                'header' => Yii::t('ImageModule.image', 'Gallery'),
                'type'   => 'raw',
                'filter' => $model->galleryList(),
                'value'  => '$data->galleryName === null
                            ? "---"
                            : CHtml::link(
                                $data->gallery->name,
                                Yii::app()->controller instanceof yupe\components\controllers\BackController
                                ? array("/gallery/galleryBackend/update", "id" => $data->galleryId)
                                : array("/gallery/gallery/update", "id" => $data->galleryId)
                            )',
            ),
            'name',
            'alt',
            array(
                'class'       => 'bootstrap.widgets.TbButtonColumn',
                'htmlOptions' => array(
                    'style'   => 'width: 60px;'
                ),
            ),
        ),
    )
); ?>