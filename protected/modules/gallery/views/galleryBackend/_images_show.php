<?php
/**
 * Отображение для Default/_show_images:
 * 
 *   @category YupeView
 *   @package  yupe
 *   @author   JetStyle <hamsterman@jetstyle.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
?>
<div id="gallery-wrapper">
    <?php $this->widget('gallery.widgets.GalleryWidget', array('galleryId' => $model->id, 'gallery' => $model, 'limit' => 30)); ?>
</div>