<?php

$this->breadcrumbs = array(
    Yii::app()->getModule('gallery')->getCategory() => array(),
    Yii::t('GalleryModule.gallery', 'Galleries') => array('/gallery/galleryBackend/index'),
    Yii::t('GalleryModule.gallery', 'Management'),
);

$this->pageTitle = Yii::t('GalleryModule.gallery', 'Galleries - manage');

$this->menu = array(
    array('icon' => 'list-alt', 'label' => Yii::t('GalleryModule.gallery', 'Gallery management'), 'url' => array('/gallery/galleryBackend/index')),
    array('icon' => 'plus-sign', 'label' => Yii::t('GalleryModule.gallery', 'Create gallery'), 'url' => array('/gallery/galleryBackend/create')),
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('GalleryModule.gallery', 'Galleries'); ?>
        <small><?php echo Yii::t('GalleryModule.gallery', 'management'); ?></small>
    </h1>
</div>

<?php $this->widget(
    'yupe\widgets\CustomGridView', array(
        'id'           => 'gallery-grid',
        'type'         => 'condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => array(
            'id',
            'name',
            array(
                'name'   => 'owner',
                'filter' => $model->usersList,
                'value'  => '$data->ownerName',
            ),
            array(
                'type' => 'html',
                'name'  => 'description',
            ),
            array(
                'name'  => 'status',
                'value' => '$data->getStatus()'
            ),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{images}{view}{update}{delete}',
                'buttons'  => array(
                    'images' => array(
                        'icon'     => 'picture',
                        'label'    => Yii::t('GalleryModule.gallery', 'Gallery images'),
                        'url'      => 'array("/gallery/galleryBackend/images", "id" => $data->id)',
                    ),
                ),
                'htmlOptions' => array(
                    'style'   => 'width: 60px; text-align: right;'
                )
            ),
        ),
    )
); ?>