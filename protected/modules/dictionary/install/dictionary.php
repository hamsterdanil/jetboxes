<?php
/**
 * Файл конфигурации для модуля
 *
 * @category YupeMigration
 * @package  yupe.modules.dictionary.install
 * @author   YupeTeam <hamsterman@jetstyle.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://jetstyle.ru
 *
 **/
return array(
    'module'   => array(
        'class' => 'application.modules.dictionary.DictionaryModule',
    ),
    'import'    => array(),
    'component' => array(),
    'rules'     => array(),
);