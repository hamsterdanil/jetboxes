<?php
/**
 * Файл настроек для модуля
 *
 * @author JetStyle <hamsterman@jetstyle.ru>
 * @link http://jetstyle.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.blog.install
 * @since 0.1
 *
 */
return array(
    'module'   => array(
        'class' => 'application.modules.blog.BlogModule',
    ),
    'import'    => array(
        'application.modules.blog.models.*',
    ),
    'component' => array(),
    'rules'     => array(
        '/post/<slug>.html' => 'blog/post/show',
        '/posts/tag/<tag>'  => 'blog/post/list',
        '/blogs/<slug>'     => 'blog/blog/show',
        '/blogs'            => 'blog/blog/index',
    ),
);