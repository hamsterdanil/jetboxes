<?php

/**
 * BlogsWidget виджет для вывода блогов
 *
 * @author JetStyle <hamsterman@jetstyle.ru>
 * @link http://jetstyle.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.blog.widgets
 * @since 0.1
 *
 */

class BlogsWidget extends YWidget
{
    public $view = 'blogswidget';

    public function run()
    { 
        $this->render($this->view, array('models' => Blog::model()->public()->published()->with('membersCount','postsCount')->cache($this->cacheTime)->findAll(array('limit' => $this->limit))));
    }
}
