<?php

/**
 * ShareWidget виджет для вывода кнопок "поделиться"
 *
 * @author JetStyle <hamsterman@jetstyle.ru>
 * @link http://jetstyle.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.blog.widgets
 * @since 0.1
 *
 */

class ShareWidget extends YWidget
{
    public $view = 'share';

    public function run()
    {
        $this->render($this->view);
    }
}