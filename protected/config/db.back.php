<?php
 return array (
  'class' => 'CDbConnection',
  'connectionString' => 'mysql:host=mysql.jetstyle.ru;port=3306;dbname=yupe',
  'username' => 'developer',
  'password' => 'd3v3l0p3r',
  'emulatePrepare' => true,
  'charset' => 'utf8',
  'enableParamLogging' => false, //defined('YII_DEBUG') && YII_DEBUG ? true : 0,
  'enableProfiling' => false, //defined('YII_DEBUG') && YII_DEBUG ? true : 0,
  'schemaCachingDuration' => 108000,
  'tablePrefix' => 'orgon_',
);
