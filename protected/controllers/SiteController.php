<?php

class SiteController extends yupe\components\controllers\FrontController
{
    const POST_PER_PAGE = 5;

    public function actionModern()
    {
        $this->render('modern');
    }

    public function actionIndex()
    {
        $this->render('welcome');
    }

    public function actionError()
    {
        $error = Yii::app()->errorHandler->error;

        if (empty($error) || !isset($error['code']) || !(isset($error['message']) || isset($error['msg']))) {
            $this->redirect(array('index'));
        }

        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo json_encode(
                $error
            );
        } else {            
            $this->render(
                'error',
                array(
                    'error' => $error
                )
            );
        }
    }


    public function actionMain()
    {
        $dataProvider = new CActiveDataProvider('Post', array(
            'criteria' => new CDbCriteria(array(
                'condition' => 't.status = :status',
                'params'    => array(':status' => Post::STATUS_PUBLISHED),
                'limit'     => self::POST_PER_PAGE,
                'order'     => 't.id DESC',
                'with'      => array('createUser', 'blog','commentsCount'),
            )),
        ));

        $this->render('main', array('dataProvider' => $dataProvider));
    }

    public function actionBrowse() {
        // я создал специально отдельный пустой лейаут
        $this->layout='//layouts/clear';
        $this->render('browse');
    }
}