<?php

class ElfinderController extends CController
{ 
    public function actions()
    {
        return array(
            'connector' => array(
                'class' => 'ext.elFinder.ElFinderConnectorAction',
                'settings' => array(
                    'root' => Yii::getPathOfAlias('webroot') . '/uploads/',
                    'URL' => Yii::app()->baseUrl . '/uploads/',
                    'rootAlias' => 'Home',
                    'mimeDetect' => 'none',
                    'debug' => true,
                    'roots' => array(
                        array(
                            'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
                            'root' => Yii::getPathOfAlias('webroot') . '/uploads/',
                            'path' => Yii::getPathOfAlias('webroot') . '/uploads/',
                            'URL' => Yii::app()->baseUrl . '/uploads/',
                            'accessControl' => 'access'             // disable and hide dot starting files (OPTIONAL)
                        )
                    )
                )
            ),
        );
    }
}