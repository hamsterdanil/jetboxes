﻿/*
Copyright (c) 2003-2009, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.addStylesSet('default',[
{
    name:'Заголовок 2',
    element:'h2',
    attributes:{
        style:"font-size: 24px !important; font-weight: bold !important; color: #025851 !important; line-height: 1 !important; margin: 1em 0 !important; margin-top: 0 !important;"
    }
},
{
    name:'Заголовок 3',
    element:'h3',
    attributes:{
        style:"font-size: 19px !important; font-weight: bold !important; color: #000000 !important; line-height: 1 !important; margin: 1em 0 !important;"
    }
},
{
    name:'Цитата',
    element:'blockquote',
    attributes:{
        style:"border: 1px dashed #4bac9c !important; margin: 1em 0 !important; padding: 10px !important; background: #ffffff !important;"
    }
},
{
    name:'Цитата курсивом',
    element:'blockquote',
    attributes:{
        class:"t-tailed-quote",
        style:"background: #f8fdf7; position: relative; font-style: italic; color: #025851; border: 1px dashed #4bac9c; margin: 10px 0; padding: 15px 20px;"
    }
},{name:'Big',element:'big'},{name:'Small',element:'small'},{name:'Typewriter',element:'tt'},{name:'Computer Code',element:'code'},{name:'Keyboard Phrase',element:'kbd'},{name:'Sample Text',element:'samp'},{name:'Variable',element:'var'},{name:'Deleted Text',element:'del'},{name:'Inserted Text',element:'ins'},{name:'Cited Work',element:'cite'},{name:'Inline Quotation',element:'q'},{name:'Language: RTL',element:'span',attributes:{dir:'rtl'}},{name:'Language: LTR',element:'span',attributes:{dir:'ltr'}},{name:'Image on Left',element:'img',attributes:{style:'padding: 5px; margin-right: 5px',border:'2',align:'left'}},{name:'Image on Right',element:'img',attributes:{style:'padding: 5px; margin-left: 5px',border:'2',align:'right'}}]);
