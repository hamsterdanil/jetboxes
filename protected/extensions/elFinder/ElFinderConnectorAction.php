<?php
/**
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 */
class ElFinderConnectorAction extends CAction
{
    /**
     * https://github.com/Studio-42/elFinder/wiki/Connector-configuration-options
     * @var array
     */
    public $settings = array();

    public function run()
    {
        error_reporting(0); // Set E_ALL for debuging

        include_once dirname(__FILE__) . '/php/elFinderConnector.class.php';
        include_once dirname(__FILE__) . '/php/elFinder.class.php';
        include_once dirname(__FILE__) . '/php/elFinderVolumeDriver.class.php';
        //include_once dirname(__FILE__) . '/php/elFinderVolumeLocalFileSystem.class.php';
        // Required for MySQL storage connector
        // include_once dirname(__FILE__).'/php/elFinderVolumeMySQL.class.php';
        // Required for FTP connector support
        // include_once dirname(__FILE__).'/php/elFinderVolumeFTP.class.php';


        /**
         * Simple function to demonstrate how to control file access using "accessControl" callback.
         * This method will disable accessing files/folders starting from  '.' (dot)
         *
         * @param  string  $attr  attribute name (read|write|locked|hidden)
         * @param  string  $path  file path relative to volume root directory started with directory separator
         * @param $data
         * @param $volume
         * @return bool|null
         */
        function access($attr, $path, $data, $volume)
        {
            return strpos(basename($path), '.') === 0 // if file/folder begins with '.' (dot)
                ? !($attr == 'read' || $attr == 'write') // set read+write to false, other (locked+hidden) set to true
                : null; // else elFinder decide it itself
        }

        $opts = $this->settings;
        foreach ($opts['roots'] as &$r) {
            switch ($r['driver']) {
                case 'LocalFileSystem':
                    include_once dirname(__FILE__) . '/php/elFinderVolumeLocalFileSystem.class.php';
                    break;
                case 'MySQL':
                    include_once dirname(__FILE__) . '/php/elFinderVolumeMySQL.class.php';
                    break;
                case 'FTP':
                    include_once dirname(__FILE__) . '/php/elFinderVolumeFTP.class.php';
                    break;
            }
            $r['accessControl'] = 'access'; // disable and hide dot starting files (OPTIONAL)
        }
        $connector = new elFinderConnector(new elFinder($opts));
        $connector->run();

    }
}
