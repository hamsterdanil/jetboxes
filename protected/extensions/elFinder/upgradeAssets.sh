#!/usr/bin/env sh

cd ./vendors/elFinder/
jake -C ./build clean
jake -C ./build elfinder
cp -r ./build/css ./../../assets/
cp -r ./build/js ./../../assets/
cp -r ./build/img ./../../assets/
cp -r ./build/php ./../../
jake -C ./build clean
cd ../../