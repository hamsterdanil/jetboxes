$(function() {

	var closeDropdownEvent = "bDropdownClose";
	var openDropdownEvent  = "bDropdownOpen";

	function closeDropdowns() {
		$(".js-dropdown").find(".b-dropdown__content").slideUp();
		$(".b-dropdown").trigger(closeDropdownEvent);
	}

	$(document).on("click", ".js-dropdown .b-dropdown__row",  function() {
		var self = $(this);
		var dropdown = self.closest(".b-dropdown");
		var content = dropdown.find(".b-dropdown__content");

		if (content.is(":visible")) {
			dropdown.trigger(closeDropdownEvent);
		} else {
			dropdown.trigger(openDropdownEvent);
		}
		content.stop(true,false).slideToggle();
		return false;
	});

	$(document).on("click", function() {
		closeDropdowns();
	});
	$(document).on("keyup", function(e) {
		if (e.keyCode === 27) {
			closeDropdowns();
		}
	});
});