$(function() {

	$(document).on("click", ".js-view__edit", function(e) {
		var self = $(this);
		self.trigger("setFormModeEdit", self);
		e.preventDefault();
	});
	$(document).on("click", ".js-view__view", function(e) {
		var self = $(this);
		self.trigger("setFormModeView", self);
		e.preventDefault();
	});

	$(document).on("setFormModeEdit", ".b-view", function(e, obj) {
		$(obj).closest(".b-view")
			.addClass("b-view_edit")
			.removeClass("b-view_view");
		e.preventDefault();
	});
	$(document).on("setFormModeView", ".b-view", function(e, obj) {
		$(obj).closest(".b-view")
			.addClass("b-view_view")
			.removeClass("b-view_edit");
		e.preventDefault();
	});

});