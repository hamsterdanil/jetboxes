function initCustomInput() {
	var input = $(this);

	// get the associated label using the input's id
	var label = $('label[for="'+input.attr('id')+'"]');

	// wrap the input + label in a div 
	input.add(label).wrapAll('<div class="b-customInput_'+ input.attr('type') +'"></div>');

	label.hover(
		function(){ $(this).addClass('b-customInput__label_hover'); },
		function(){ $(this).removeClass('b-customInput__label_hover'); }
	);

	//bind custom event, trigger it, bind click,focus,blur events
	input.bind('updateState', function(){
		if (input.is(':checked')){
			label.addClass('b-customInput__label_checked');
		} else {
			label.removeClass('b-customInput__label_checked b-customInput__label_checkedHover b-customInput__label_checkedFocus');
		}
	})
	.trigger('updateState')
	.click(function(){
		$('input[name="'+ $(this).attr('name') +'"]').trigger('updateState');
	})
	.focus(function(){
		label.addClass('b-customInput__label_focus');
		if(input.is(':checked')){
			$(this).addClass('b-customInput__checkedFocus');
		}
	})
	.blur(function(){ label.removeClass('b-customInput__label_focus b-customInput__label_checkedFocus'); });
}

var bCustomInputs = {
	init: function() {
		$('.b-customInput').each(initCustomInput);
	}
};

bCustomInputs.init();
