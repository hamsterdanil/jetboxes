$(function() {

	$(document).on("click", ".b-health__about-organ__toggle", function() {
		var self = $(this);
		var organsAbout = self.closest(".b-health__about-organs");

		if(organsAbout.hasClass("b-health__about-organs_closed")){
			self.text("Скрыть описание органов");
			organsAbout.removeClass("b-health__about-organs_closed");
		} else {
			self.text("Показать описание органов");
			organsAbout.addClass("b-health__about-organs_closed");
		}

		return false;
	});
	$(document).on("click", ".js-health-list_organs .b-health-list__link", function() {
		var self = $(this);
		var containerItem = self.closest(".b-health-list__item");
		var clickedOrganId = containerItem.attr("data-organ-id");

		$("#b-health__about-organs").find(".b-health__about-organ_hidden").remove();

		if (self.hasClass("b-health-list__link_selected")) {
			// deselect
			self.removeClass("b-health-list__link_selected");

			var newOrganAbout = $("#b-health__about-organs").find(".b-health__about-organ")
				.filter(function() {
					return $(this).attr("data-organ-id") === clickedOrganId
				})
			newOrganAbout.addClass("b-health__about-organ_hidden");
		} else {
			// select organ add description
			self.addClass("b-health-list__link_selected");

			var data = {
				organ_id:     clickedOrganId,
				organ_hp:     containerItem.find(".b-health-list__hp").text(),
				organ_safety: containerItem.find(".b-health-list__safety").text(),
				organ_name:   containerItem.find(".b-health-list__link").text(),
				organ_text:   containerItem.find(".b-health-list__about-organ-data").text() // html ?
			}

			$("#b-health__about-organs .b-health__about-organ__toggle").after(tmpl("organ_about_template", data));

			var newOrganAbout = $("#b-health__about-organs")
				.find(".b-health__about-organ")
				.filter(function() {
					return $(this).attr("data-organ-id") === clickedOrganId
				});

			setTimeout(function() {
				newOrganAbout.removeClass("b-health__about-organ_hidden");
			}, 1);
		}

		return false;
	});
});