$(document).ready(function() {
	// $(".b-select").dropkick();

	$(".js-select_options_people1_day").dropkick();
	$(".js-select_options_people1_month").dropkick();
	$(".js-select_options_people1_year").dropkick();

	$(".js-select_options_timezone").dropkick({
		change: function (value, label) {
			// change timezone ajax
			console.log(value, label);
		}
	});

	$(".js-select_measures").dropkick();
	$(".js-select_measures-count").dropkick();

	$(".js-select_user1_day").dropkick();
	$(".js-select_user1_month").dropkick();
	$(".js-select_user1_year").dropkick();
	$(".js-select_user2_day").dropkick();
	$(".js-select_user2_month").dropkick();
	$(".js-select_user2_year").dropkick();

	$(".js-select_adduser_day").dropkick();
	$(".js-select_adduser_month").dropkick();
	$(".js-select_adduser_year").dropkick();

});