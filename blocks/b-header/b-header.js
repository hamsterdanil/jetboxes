$(function() {
	var bDropdown = $(".b-header__dropdown");
	var closeDropdownEvent = "bDropdownClose";
	var openDropdownEvent  = "bDropdownOpen";

	bDropdown.on(openDropdownEvent, function() {
		var headerAccount = $(".b-header__account");
		headerAccount
			.addClass("b-header__account_open")
			.removeClass("b-header__account_closed");
	});
	bDropdown.on(closeDropdownEvent, function() {
		var headerAccount = $(".b-header__account");
		headerAccount
			.addClass("b-header__account_closed")
			.removeClass("b-header__account_open");
	});
});