/*global tmpl:true*/
var bTabs = {
	selectTab: function(index) {
		$(".b-tab-tip").removeClass("b-tab_current");
		$(".b-tab-tip").eq(index).addClass("b-tab_current");

		var currentPage = $(".b-tab__pages .b-tab__page").eq(index);
		$(".b-tab__pages .b-tab__page").not(currentPage).hide();
		currentPage.show();
	},

	addTab: function(title) {
		if (bTabs.isTabWithTitleExists(title)) { return; }

		var data = {
			title: title,
			text: "Страница инстркций заглушка"
		};
		$(".b-tab-add-link").before(tmpl("newtab_template", data));
		$("#b-tab__pages").append(tmpl("newpages_template", data));
	},

	isTabWithTitleExists: function(title) {
		return $(".b-tab-tip .b-tab__link").filter(function() {
			return $(this).text() === title;
		}).length;
	}
};

$(function() {

	$(document).on("click", ".b-tab-tip .b-tab__link", function(e) {
		var self = $(this);
		var tab  = self.closest(".b-tab-tip");
		var index = tab.index();

		bTabs.selectTab(index);

		e.preventDefault();
	});

});
