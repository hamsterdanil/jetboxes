var bGraph = {

};

var bGraphUtils = {
  getColor: function(val) {
    var color;

    if (val === 5) {
      color = "#94c400";
    } else if (val === 4) {
      color = "#bcd300";
    } else if (val === 3) {
      color = "#fecc09";
    } else if (val === 2) {
      color = "#f69700";
    } else if (val === 1) {
      color = "#f75c00";
    } else {
      throw new Error("getColor only receiving numbers between 1 to 5");
    }
    return color;
  },
  // returns bar left side x-coords
  getXFromDayNumber: function(number) {
  	var barWidth = 6;
    var barGutter = 20;

    if (number<=1){ return 0; }
    return (barGutter + barWidth) * (number-1);
  }
}