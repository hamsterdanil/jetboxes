/*global Raphael:true*/
$(function () {
  var gw = 1420;

  //- viewport width
  var barsGraphVw = 1420;
  //- viewport height
  var barsGraphVh = 81;
  //- top offset so there is space; 
  var barsGraphYOffset = 46;
  var barsGraphZeroLine = barsGraphVh+barsGraphYOffset;
  //- base colors
  var green  = "#9eba00";
  var yellow = "#e3c600";
  var red    = "#ff5400";

  var bars = [];
  var barWidth = 6;
  var barStartOpacity = 1;
  var barEndOpacity = 0.2;

  var rAxisLabels = [];

  var dayTickOffset = 6;
  var dayTickHeight = 8;

  var monthUnderlineY = barsGraphYOffset+barsGraphVh+dayTickOffset+dayTickHeight-1; // -1 because line width 2px
  var barsDayLabelY   = monthUnderlineY + 14;
  var barsMonthLabelY = monthUnderlineY + 43;

  var maxValue = 5;

  var legendaLeftOffset = 50;

  var graphPaper;
  var rightLabelPaper;

  var shadowSet;

  var daysCount = 0;

  var myScroll;

  var healthJSON = [
    {
      label: "Апрель 2013",
      days: [
        { day: 1, val: 3,   url: "measurements.html/04/1" },
        { day: 2, val: 3,   url: "measurements.html/04/2" },
        { day: 3, val: null, url: "measurements.html/04/3" },
        { day: 5, val: 4,   url: "measurements.html/ololo" }
      ]
    },
    {
      label: "Май 2013",
      days: [
        { day: 1,  val: 1,   url: "measurements.html" },
        { day: 2,  val: 1,   url: "measurements.html" },
        { day: 3,  val: 1,   url: "measurements.html" },
        { day: 4,  val: 1,   url: "measurements.html" },
        { day: 5,  val: 1,   url: "measurements.html" },
        { day: 6,  val: 1,   url: "measurements.html" },
        { day: 7,  val: 2,   url: "measurements.html" },
        { day: 8,  val: 2,   url: "measurements.html" },
        { day: 9,  val: 2,   url: "measurements.html" },
        { day: 10, val: 2,   url: "measurements.html" },
        { day: 11, val: 2,   url: "measurements.html" },
        { day: 12, val: 2,   url: "measurements.html" },
        { day: 13, val: 3,   url: "measurements.html" },
        { day: 14, val: null, url: "measurements.html" },
        { day: 15, val: 3,   url: "measurements.html" },
        { day: 16, val: 3,   url: "measurements.html" },
        { day: 17, val: 3,   url: "measurements.html" },
        { day: 18, val: 3,   url: "measurements.html" },
        { day: 19, val: 4,   url: "measurements.html" },
        { day: 20, val: 4,   url: "measurements.html" },
        { day: 21, val: 4,   url: "measurements.html" },
        { day: 22, val: 4,   url: "measurements.html" },
        { day: 23, val: 4,   url: "measurements.html" },
        { day: 24, val: 5,   url: "measurements.html" },
        { day: 25, val: 5,   url: "measurements.html" }
      ]
    },
    {
      label: "Июнь 2013",
      days: [
        { day: 1,  val: 3,   url: "journal.html" },
        { day: 2,  val: 2,   url: "journal.html" },
        { day: 4,  val: null, url: "journal.html" },
        { day: 5,  val: 4,   url: "journal.html" },
        { day: 7,  val: 4,   url: "journal.html" },
        { day: 9,  val: 2,   url: "journal.html" },
        { day: 13, val: 2,   url: "journal.html" }
      ]
    },
    {
      label: "Июль 2013",
      days: [
        { day: 1,  val: 1,    url: "measurements.html" },
        { day: 2,  val: 1,    url: "measurements.html" },
        { day: 3,  val: null, url: "measurements.html" },
        { day: 8,  val: 2,   url: "measurements.html" },
        { day: 9,  val: 2,   url: "measurements.html" },
        { day: 10, val: 2,   url: "measurements.html" },
        { day: 13, val: 3,   url: "measurements.html" },
        { day: 14, val: null, url: "measurements.html" },
        { day: 15, val: null, url: "measurements.html" },
        { day: 16, val: 3,   url: "measurements.html" },
        { day: 17, val: 3,   url: "measurements.html" },
        { day: 18, val: 3,   url: "measurements.html" },
        { day: 19, val: 4,   url: "measurements.html" },
        { day: 20, val: null, url: "measurements.html" },
        { day: 21, val: 3,   url: "measurements.html" },
        { day: 24, val: 2,   url: "measurements.html" },
        { day: 25, val: null, url: "measurements.html" }
      ]
    },
  ];





  if ($("#b-graph-bars__paper").length && $("#b-graph-bars__axis").length){
    prepareBarsGraphData( healthJSON );
    drawBarsGraphAxis( rightLabelPaper );
    drawBarsGraphValueLabels( rightLabelPaper );
    drawBarsGraph( healthJSON );

    myScroll = new IScroll('#b-graph-bars__paper', {
      scrollX: true,
      scrollY: false,
      mouseWheel: true,
      bounceTime: 350
    });

    $(document).on("click", ".js-graph-bars__arrow_l", function(e) {
      myScroll.scrollBy(75, 0, 500);
      e.preventDefault();
    });
    $(document).on("click", ".js-graph-bars__arrow_r", function(e) {
      myScroll.scrollBy(-75, 0, 500);
      e.preventDefault();
    });
  }

  function onBarMouseIn() {
    for (var i = bars.length; i--;) {
      if (this === bars[i]){
        this.animate({ "opacity": barStartOpacity }, 300);
      } else {
        bars[i].animate({ "opacity": barEndOpacity }, 500, "easeOut");
      }
    }
    this.flag = graphPaper.popup(this.attrs.x+this.attrs.width/2, this.attrs.y-5, parseInt(this.val, 10) || "0", "up", 8).insertBefore(this);
    this.flag[0].attr("fill", "#fff");

    // shadow imitation
    shadowSet.push( this.flag[0].glow({color: "#bbb", width: 6}) );
    this.flag[1]
      .attr("fill", this.attrs.fill)
      .attr({
        "font-size": 18,
        "font-family": "'Calibri', 'Lucida Grande', Lucida, sans-serif",
        "font-weight": "normal",
        "webkit-font-smoothing": "antialiased"
      }).toFront();
  }

  function onBarMouseOut() {
    for (var i = bars.length; i--;) {
      bars[i].animate({ "opacity": barStartOpacity }, 500, "easeOut");
    }
    this.flag.animate({opacity: 0}, 300, function () { this.remove(); });
    // shadow imitation
    shadowSet.remove();
  }


  function drawPolygonByPoints(color, pointsArray){
    var lastPointY = pointsArray.slice(-2, -1); // y,x,y,x, >y< ,x

    // add bottom left point
    pointsArray.unshift( barsGraphZeroLine );
    pointsArray.unshift( 0 );

    // add bottom right point
    pointsArray.push( lastPointY );
    pointsArray.push( barsGraphZeroLine );

    graphPaper
      .path("M " + pointsArray.join(" ") + " Z")
      .attr({"fill": color, "stroke-width": "0", "opacity": "0.7"})
      .toBack();
      // .glow({color: "#ccc", width: 4});
  }

  function drawHorizLine(paper, x, y, width, color, strokeWidth) {
    x = x || 0;
    strokeWidth = strokeWidth || "1";
    paper.path("M"+x+" "+y+" "+width+" "+y)
      .attr("stroke-width", strokeWidth)
      .attr("stroke", color);
  }

  function drawBar(x, y, width, height) {
    var normHeight = getNormalizedHeight(height);
    var rect = graphPaper.rect(x, y-normHeight, width, normHeight)
      .attr({"fill": bGraphUtils.getColor(height), "stroke":"none", "opacity": barStartOpacity});
    return rect;
  }
  function drawDayBar(dayNumber, val) {
    var bar = drawBar(bGraphUtils.getXFromDayNumber(dayNumber), barsGraphZeroLine, barWidth, val);
    bar.hover(onBarMouseIn, onBarMouseOut);

    bar.val = val;
    bar.attr({"cursor": "pointer"});
    bars.push(bar);
    return bar;
  }

  function drawBarsGraphAxis(paper){
    var axisX = barsGraphVw - 26;
    drawHorizLine(paper, 0, barsGraphYOffset+0,       axisX, green);
    drawHorizLine(paper, 0, barsGraphYOffset+barsGraphVh*0.2, axisX, "#d1d7c0");
    drawHorizLine(paper, 0, barsGraphYOffset+barsGraphVh*0.4, axisX, yellow);
    drawHorizLine(paper, 0, barsGraphYOffset+barsGraphVh*0.6, axisX, "#d1d7c0");
    drawHorizLine(paper, 0, barsGraphYOffset+barsGraphVh*0.8, axisX, red);
    drawHorizLine(paper, 0, barsGraphYOffset+barsGraphVh*1, axisX, "#d1d7c0");
  }

  function drawBarsGraphValueLabels(paper){
    var labelX = barsGraphVw-20;
    rAxisLabels.push( paper.text(labelX, barsGraphYOffset+0,      "5") );
    rAxisLabels.push( paper.text(labelX, barsGraphYOffset+barsGraphVh*0.2, "4") );
    rAxisLabels.push( paper.text(labelX, barsGraphYOffset+barsGraphVh*0.4, "3") );
    rAxisLabels.push( paper.text(labelX, barsGraphYOffset+barsGraphVh*0.6, "2") );
    rAxisLabels.push( paper.text(labelX, barsGraphYOffset+barsGraphVh*0.8, "1") );
    rAxisLabels.push( paper.text(labelX, barsGraphYOffset+barsGraphVh*1,   "0") );

    for (var i = 0; i < rAxisLabels.length; i++) {
      rAxisLabels[i].attr({
        "text-anchor": "start",
        "font-size": 10,
        "fill": "#8D916F",
        "font-family": "'Calibri', 'Lucida Grande', Lucida, sans-serif",
        "webkit-font-smoothing": "antialiased"
      });
    }
  }

  function drawBarsDayTick(startDayNumber){
    graphPaper.rect( bGraphUtils.getXFromDayNumber(startDayNumber), barsGraphZeroLine+dayTickOffset, barWidth, dayTickHeight )
        .attr({"fill": "#c9c9b7", "stroke":"none"});
  }

  function drawBarsMonthGroupLine(startDayNumber, endDayNumber){
    drawHorizLine(graphPaper, bGraphUtils.getXFromDayNumber(startDayNumber), monthUnderlineY, bGraphUtils.getXFromDayNumber(endDayNumber - 1), "#c9c9b7", "2"); // lines inbetween => count-1
  }

  function drawBarsMonthLabel(startDayNumber, labelTxt){
    graphPaper.text(bGraphUtils.getXFromDayNumber(startDayNumber), barsMonthLabelY, labelTxt)
      .attr({
        "text-anchor": "start",
        "font-size": 12,
        "font-weight": "bold",
        "fill": "#8D916F",
        "font-family": "'Calibri', 'Lucida Grande', Lucida, sans-serif",
        "webkit-font-smoothing": "antialiased"
      });
  }

  function drawBarsDayLabel(startDayNumber, labelTxt){
    graphPaper.text(bGraphUtils.getXFromDayNumber(startDayNumber), barsDayLabelY, labelTxt)
      .attr({
        "text-anchor": "start",
        "font-size": 11,
        "fill": "#8D916F",
        "font-family": "'Calibri', 'Lucida Grande', Lucida, sans-serif",
        "webkit-font-smoothing": "antialiased"
      });
  }

  function drawBarsGraph(jsonData){
    var monthCount = jsonData.length;
    var dayNumber = 1;
    var points = [];

    for (var i = 0; i < monthCount; i++) {
      var month = jsonData[i];
      var daysInMonth = month.days.length;

      drawBarsMonthGroupLine(dayNumber, dayNumber+daysInMonth);
      drawBarsMonthLabel(dayNumber, month.label);

      for (var j = 0; j < daysInMonth; j++) {
        var day = month.days[j];

        drawBarsDayTick(dayNumber);

        if (day.val !== null){
          points.push( bGraphUtils.getXFromDayNumber(dayNumber)+barWidth/2 );
          points.push( barsGraphZeroLine - getNormalizedHeight(day.val) );

          var bar = drawDayBar(dayNumber, day.val);
          $(bar).attr("data-url", day.url);
          bar.click(function() {
            location.href = $(this).attr("data-url");
          });
          drawBarsDayLabel(dayNumber, day.day);
        }

        dayNumber += 1;
      }
    }

    drawPolygonByPoints("#e2e5db", points);
  }

  function prepareBarsGraphData(jsonData) {
    for (var i = 0; i < jsonData.length; i++) {
      daysCount += jsonData[i].days.length;
    }
    var dataWidth = bGraphUtils.getXFromDayNumber(daysCount) + barWidth + legendaLeftOffset;
    barsGraphVw   = Math.max(barsGraphVw, dataWidth);
    graphPaper      = Raphael("b-graph-bars__paper", dataWidth, 191);
    rightLabelPaper = Raphael("b-graph-bars__axis", gw, 191);

    shadowSet = graphPaper.set();
  }

  function getNormalizedHeight(value){
    return value/maxValue * barsGraphVh;
  }
});