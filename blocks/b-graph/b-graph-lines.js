/*global Raphael:true, IScroll:true*/
$(function () {
  var gw = 1420;

  var linesGraphHeight = 500;
  //- viewport width
  var linesGraphVw = 1420;
  //- viewport height
  var linesGraphVh = 246;
  var linesGraphYOffset = 4;
  var linesGraphZeroLine = linesGraphVh+linesGraphYOffset;
  // lines graph base colors

  var redColor    = "#f75c00";
  var orangeColor = "#f69700";
  var yellowColor = "#fecc09";
  var lgreenColor = "#bcd300";
  var greenColor  = "#94c400";


  var barWidth = 6;

  var rAxisLabels = [];

  var dayTickOffset = 17;
  var dayTickHeight = 9;

  var monthUnderlineY  = dayTickOffset+dayTickHeight-1; // -1 because line width 2px
  var linesDayLabelY   = monthUnderlineY + 11;
  var linesMonthLabelY = monthUnderlineY + 40;

  var maxValue = 5;

  var daysCount = 0;

  var circles = [];

  var kidneyHealthJSON = [
    {
      label: "Апрель 2013",
      days: [
        {day: 1, val: 4},
        {day: 2, val: 3},
        {day: 3, val: 3},
        {day: 5, val: 4}
      ]
    },
    {
      label: "Май 2013",
      days: [
        {day: 1, val: 3},
        {day: 2, val: 2},
        {day: 3, val: 3},
        {day: 4, val: 3},
        {day: 5, val: 5},
        {day: 6, val: 3},
        {day: 7, val: 3},
        {day: 8, val: 3},
        {day: 9, val: 3},
        {day: 10, val: 3},
        {day: 11, val: 1},
        {day: 12, val: 1},
        {day: 13, val: 1},
        {day: 14, val: 5},
        {day: 15, val: 1},
        {day: 16, val: 2},
        {day: 17, val: 3},
        {day: 18, val: 4},
        {day: 19, val: 1},
        {day: 20, val: 3},
        {day: 21, val: 1},
        {day: 22, val: 4},
        {day: 23, val: 5},
        {day: 24, val: 3},
        {day: 25, val: 4}
      ]
    },
    {
      label: "Июнь 2013",
      days: [
        {day: 1, val: 5},
        {day: 2, val: 4},
        {day: 4, val: 4},
        {day: 5, val: 5},
        {day: 7, val: 5},
        {day: 9, val: 5},
        {day: 13, val: 1}
      ]
    },
  ];
  kidneyHealthJSON.label = "Почки";

  var healthJSON = [
    {
      label: "Апрель 2013",
      days: [
        {day: 1, val: 3},
        {day: 2, val: 3},
        {day: 3, val: null},
        {day: 5, val: 4}
      ]
    },
    {
      label: "Май 2013",
      days: [
        {day: 1, val: 1},
        {day: 2, val: 1},
        {day: 3, val: 1},
        {day: 4, val: 1},
        {day: 5, val: 1},
        {day: 6, val: 2},
        {day: 7, val: 2},
        {day: 8, val: 2},
        {day: 9, val: 2},
        {day: 10, val: 3},
        {day: 11, val: 2},
        {day: 12, val: 2},
        {day: 13, val: 3},
        {day: 14, val: null},
        {day: 15, val: 1},
        {day: 16, val: 2},
        {day: 17, val: 2},
        {day: 18, val: 2},
        {day: 19, val: 1},
        {day: 20, val: 2},
        {day: 21, val: 3},
        {day: 22, val: 2},
        {day: 23, val: 2},
        {day: 24, val: 2},
        {day: 25, val: 2}
      ]
    },
    {
      label: "Июнь 2013",
      days: [
        {day: 1, val: 3},
        {day: 2, val: 2},
        {day: 4, val: null},
        {day: 5, val: 4},
        {day: 7, val: 4},
        {day: 9, val: 2},
        {day: 13, val: 2}
      ]
    },
    {
      label: "Июль 2013",
      days: [
        {day: 1, val: 1},
        {day: 2, val: 1},
        {day: 3, val: null},
        {day: 8, val: 2},
        {day: 9, val: 2},
        {day: 10, val: 2},
        {day: 13, val: 3},
        {day: 14, val: null},
        {day: 15, val: null},
        {day: 16, val: 3},
        {day: 17, val: 3},
        {day: 18, val: 3},
        {day: 19, val: 4},
        {day: 20, val: null},
        {day: 21, val: 3},
        {day: 24, val: 2},
        {day: 25, val: 2}
      ]
    },
  ];
  healthJSON.label = "Простата";


  if ($("#b-graph-lines__paper").length && $("#b-graph-lines__axis").length && $("#b-graph-lines__axis-l").length){
    prepareLinesGraphData( healthJSON );

    var graphs = initGraphs("b-graph-lines__paper", "b-graph-lines-bg__paper", "b-graph-lines__axis", "b-graph-lines__axis-l", "b-graph-lines__axis-b");
    var graphPaper       = graphs[0];
    var bgGraphPaper     = graphs[1];
    var rightLabelPaper  = graphs[2];
    var leftLabelPaper   = graphs[3];
    var bottomLabelPaper = graphs[4];

    leftLabelZoom  =       leftLabelPaper.panzoom({ minZoom: -30 });
    rightLabelZoom =      rightLabelPaper.panzoom({ minZoom: -30 });
    bottomLabelPanZoom = bottomLabelPaper.panzoom({ minZoom: -30 });
    bgGraphZoom =            bgGraphPaper.panzoom({ minZoom: -30 });

    leftLabelZoom.enable();
    rightLabelZoom.enable();
    bottomLabelPanZoom.enable();
    bgGraphZoom.enable();

    graphPanZoom = graphPaper.panzoom({
      minZoom: -30,
      interactive: true,
      onZoom: function(zoom, zoomCenter) {
        var graphLabelZoomDiff = graphPanZoom.getCurrentZoom() - leftLabelZoom.getCurrentZoom();
        setTimeout(function() {
          if (graphLabelZoomDiff > 0) {
            leftLabelZoom.zoomInWithCenter(      graphLabelZoomDiff, {x: 0, y: zoomCenter.y} );
            rightLabelZoom.zoomInWithCenter(     graphLabelZoomDiff, {x: 0, y: zoomCenter.y} );
            bgGraphZoom.zoomInWithCenter(        graphLabelZoomDiff, zoomCenter );
            bottomLabelPanZoom.zoomInWithCenter( graphLabelZoomDiff, {x: zoomCenter.x, y: 0 } );
          } else {
            leftLabelZoom.zoomOutWithCenter(      graphLabelZoomDiff, {x: 0, y: zoomCenter.y} );
            rightLabelZoom.zoomOutWithCenter(     graphLabelZoomDiff, {x: 0, y: zoomCenter.y} );
            bgGraphZoom.zoomOutWithCenter(        graphLabelZoomDiff, zoomCenter );
            bottomLabelPanZoom.zoomOutWithCenter( graphLabelZoomDiff, {x: zoomCenter.x, y: 0 } );
          }
        }, 1);
      },
      onDragging: function() {
        var yDiff = graphPanZoom.getCurrentPosition().y - leftLabelZoom.getCurrentPosition().y;
        var tmpX = graphPanZoom.getCurrentPosition().x;

        var intersection1 = getGraphIntersectionRightSide( $("#b-graph-lines__paper"), graphsArray1[0].path, graphPanZoom.getCurrentPosition().x );
        var intersection2 = getGraphIntersectionRightSide( $("#b-graph-lines__paper"), graphsArray1[1].path, graphPanZoom.getCurrentPosition().x );

console.dir(intersection1, 'intersection1');
console.dir(intersection2, 'intersection2');

        if (legendaForGraph1 && legendaForGraph2){
          legendaForGraph1.line.transform("t0," + intersection1.y);
          legendaForGraph2.line.transform("t0," + intersection2.y);

          legendaForGraph1.label.transform("t0," + (intersection1.y-12));
          legendaForGraph2.label.transform("t0," + (intersection2.y-12));
        }

        setTimeout(function() {
          bgGraphZoom.setPosition( 0, graphPanZoom.getCurrentPosition().y );
          leftLabelZoom.setPosition( 0, graphPanZoom.getCurrentPosition().y );
          rightLabelZoom.setPosition( 0, graphPanZoom.getCurrentPosition().y );
          bottomLabelPanZoom.setPosition( graphPanZoom.getCurrentPosition().x, 0 );
        }, 1);

      }
    });
    graphPanZoom.enable();

    drawLinesGraphRows( bgGraphPaper );
    drawLinesGraphLabels( rightLabelPaper, leftLabelPaper );
    drawLinesGraphBottomLabels( bottomLabelPaper, healthJSON );

    var graphsArray1 = [
      {
        color: "#c35700",
        path:  drawLinesGraph( graphPaper, healthJSON, "#c35700"),
        label: healthJSON.label
      },
      {
        color: "#005d63",
        path:  drawLinesGraph( graphPaper, kidneyHealthJSON, "#005d63"),
        label: kidneyHealthJSON.label
      }
    ];

    var legendaForGraph1 = drawGraphLegenda(rightLabelPaper, graphsArray1[0]);
    var legendaForGraph2 = drawGraphLegenda(rightLabelPaper, graphsArray1[1]);
  }

  if ($("#b-graph-lines__paper_safety").length && $("#b-graph-lines__axis_safety").length && $("#b-graph-lines__axis-l_safety").length){
    prepareLinesGraphData( healthJSON );

    var graphs = initGraphs("b-graph-lines__paper_safety", "b-graph-lines-bg__paper_safety", "b-graph-lines__axis_safety", "b-graph-lines__axis-l_safety", "b-graph-lines__axis-b_safety");
    var graphPaperSafety       = graphs[0];
    var bgGraphPaperSafety     = graphs[1];
    var rightLabelPaperSafety  = graphs[2];
    var leftLabelPaperSafety   = graphs[3];
    var bottomLabelPaperSafety = graphs[4];

    graphPaperSafety.panzoom().enable();

    drawLinesGraphRows( bgGraphPaperSafety );
    drawLinesGraphLabels( rightLabelPaperSafety, leftLabelPaperSafety );
    drawLinesGraphBottomLabels(bottomLabelPaperSafety, healthJSON);

    var graphsArray2 = [
      {
        color: "#0000CC",
        path: drawLinesGraph( graphPaperSafety, healthJSON, "#0000CC" ),
        label: healthJSON.label
      },
      {
        color: "#990000",
        path: drawLinesGraph( graphPaperSafety, kidneyHealthJSON, "#990000"),
        label: kidneyHealthJSON.label
      }
    ];
    drawGraphLegenda(rightLabelPaperSafety, graphsArray2[0]);
    drawGraphLegenda(rightLabelPaperSafety, graphsArray2[1]);
  }



  function getGraphIntersectionRightSide(container, path, offset) {
    offset = offset || 0;
    var graphVisibleAreaWidth = container.width();
    var intersection = Raphael.pathIntersection(path, "M "+(graphVisibleAreaWidth+offset+1)+" 0 "+(graphVisibleAreaWidth+offset+1)+" "+linesGraphVh)[0];
    if (!intersection){
      var pathArray = path.split(" ");
      intersection = { y: parseFloat(pathArray[pathArray.length-1]) };
    }
    return intersection;
  }

  function drawGraphLegenda(paper, graph) {
    var legendaLine;
    var legendaText;

    var intersection =  getGraphIntersectionRightSide($("#b-graph-lines__paper"), graph.path);

    // draw at y=0 so .transform() positioning will be relative to 0
    legendaLine = drawHorizLine(paper, 0, 0, 114, graph.color, 2);
    legendaText = paper.text( 50, 0, graph.label);

    legendaLine.transform("t0," + intersection.y);
    legendaText.transform("t0," + (intersection.y-12));

    legendaText.attr({
      "text-anchor": "start",
      "font-size": 11,
      "font-weight": "bold",
      "fill": "#8D916F",
      "font-family": "'Calibri', 'Lucida Grande', Lucida, sans-serif",
      "webkit-font-smoothing": "antialiased"
    });
    return {
      line: legendaLine,
      label: legendaText
    };
  }


  function drawHorizLine(paper, x, y, width, color, strokeWidth) {
    x = x || 0;
    strokeWidth = strokeWidth || "1";
    return paper.path("M"+x+" "+y+" "+width+" "+y)
      .attr("stroke-width", strokeWidth)
      .attr("stroke", color);
  }

  function drawBgBar(paper, x, y, width, normHeight, color) {
    var rect = paper.rect(x, y-normHeight, width, normHeight)
      .attr({"fill": color, "stroke":"none"});
    return rect;
  }

  function drawLinesDayTick(paper, startDayNumber){
    paper.rect( bGraphUtils.getXFromDayNumber(startDayNumber), monthUnderlineY - dayTickHeight+1, barWidth, dayTickHeight )
        .attr({"fill": "#c9c9b7", "stroke":"none"});
  }
  function drawLinesMonthGroupLine(paper, startDayNumber, endDayNumber){
    drawHorizLine(paper, bGraphUtils.getXFromDayNumber(startDayNumber), monthUnderlineY, bGraphUtils.getXFromDayNumber(endDayNumber - 1), "#c9c9b7", "2"); // lines inbetween => count-1
  }
  function drawLinesMonthLabel(paper, startDayNumber, labelTxt){
    paper.text(bGraphUtils.getXFromDayNumber(startDayNumber), linesMonthLabelY, labelTxt)
      .attr({
        "text-anchor": "start",
        "font-size": 12,
        "font-weight": "bold",
        "fill": "#8D916F",
        "font-family": "'Calibri', 'Lucida Grande', Lucida, sans-serif",
        "webkit-font-smoothing": "antialiased"
      });
  }

  function drawLinesGraphRows(paper){
    drawBgBar(paper, 0, linesGraphZeroLine, 99999, linesGraphVh,     greenColor);
    drawBgBar(paper, 0, linesGraphZeroLine, 99999, linesGraphVh*0.8, lgreenColor);
    drawBgBar(paper, 0, linesGraphZeroLine, 99999, linesGraphVh*0.6, yellowColor);
    drawBgBar(paper, 0, linesGraphZeroLine, 99999, linesGraphVh*0.4, orangeColor);
    drawBgBar(paper, 0, linesGraphZeroLine, 99999, linesGraphVh*0.2, redColor);
  }

  function drawLinesDayLabel(paper, startDayNumber, labelTxt){
    paper.text(bGraphUtils.getXFromDayNumber(startDayNumber), linesDayLabelY, labelTxt)
      .attr({
        "text-anchor": "start",
        "font-size": 9,
        "fill": "#8D916F",
        "font-family": "'Calibri', 'Lucida Grande', Lucida, sans-serif",
        "webkit-font-smoothing": "antialiased"
      });
  }

  function drawLinesGraphLabels(paper, leftPaper){
    var leftLabelX  = 3;
    var rightLabelX = 9;

    rAxisLabels.push( leftPaper.text(leftLabelX, linesGraphYOffset+0,                "5") );
    rAxisLabels.push( leftPaper.text(leftLabelX, linesGraphYOffset+linesGraphVh*0.2, "4") );
    rAxisLabels.push( leftPaper.text(leftLabelX, linesGraphYOffset+linesGraphVh*0.4, "3") );
    rAxisLabels.push( leftPaper.text(leftLabelX, linesGraphYOffset+linesGraphVh*0.6, "2") );
    rAxisLabels.push( leftPaper.text(leftLabelX, linesGraphYOffset+linesGraphVh*0.8, "1") );
    rAxisLabels.push( leftPaper.text(leftLabelX, linesGraphYOffset+linesGraphVh,     "0") );

    rAxisLabels.push( paper.text(rightLabelX, linesGraphYOffset+0,                "5") );
    rAxisLabels.push( paper.text(rightLabelX, linesGraphYOffset+linesGraphVh*0.2, "4") );
    rAxisLabels.push( paper.text(rightLabelX, linesGraphYOffset+linesGraphVh*0.4, "3") );
    rAxisLabels.push( paper.text(rightLabelX, linesGraphYOffset+linesGraphVh*0.6, "2") );
    rAxisLabels.push( paper.text(rightLabelX, linesGraphYOffset+linesGraphVh*0.8, "1") );
    rAxisLabels.push( paper.text(rightLabelX, linesGraphYOffset+linesGraphVh,     "0") );

    for (var i = 0; i < rAxisLabels.length; i++) {
      rAxisLabels[i].attr({
        "text-anchor": "start",
        "font-size": 11,
        "fill": "#8D916F",
        "font-family": "'Calibri', 'Lucida Grande', Lucida, sans-serif",
        "webkit-font-smoothing": "antialiased"
      });
    }
  }

  function drawLinesGraphBottomLabels(paper, jsonData){
    var monthCount = jsonData.length;
    var dayNumber = 1;

    for (var i = 0; i < monthCount; i++) {
      var month = jsonData[i];
      var daysInMonth = month.days.length;

      drawLinesMonthGroupLine(paper, dayNumber, dayNumber+daysInMonth);
      drawLinesMonthLabel(paper, dayNumber, month.label);

      for (var j = 0; j < daysInMonth; j++) {
        var day = month.days[j];
        drawLinesDayTick(paper, dayNumber);

        if (day.val !== null){
          drawLinesDayLabel(paper, dayNumber, day.day);
        }

        dayNumber += 1;
      }
    }
  }

  function drawLinesGraph(paper, jsonData, color){
    var monthCount = jsonData.length;
    var dayNumber = 1;
    var points = [];

    for (var i = 0; i < monthCount; i++) {
      var month = jsonData[i];
      var daysInMonth = month.days.length;

      for (var j = 0; j < daysInMonth; j++) {
        var day = month.days[j];

        if (day.val !== null){
          points.push( bGraphUtils.getXFromDayNumber(dayNumber)+barWidth/2 );
          points.push( linesGraphZeroLine - getNormalizedHeight(day.val) );
        }

        dayNumber += 1;
      }
    }
    var linePath = drawLineByPoints(paper, points, color);

    return linePath;
  }

  function drawLineByPoints(paper, pointsArray, color){
    var linePath = "M " + pointsArray.join(" ");

    paper
      .path(linePath)
      .attr({
        "stroke": color,
        "stroke-width": "2"
      })
      .toBack();

    for (var i = 0; i < pointsArray.length; i=i+2) {
      circles.push(
        paper.circle(pointsArray[i], pointsArray[i+1], 3).attr({
          "fill": color,
          "stroke": "none",
          "cursor": "pointer"
        })
      );
    }
    return linePath;
  }

  function initGraphs(graphId, bgGraphId, rightLabelId, leftLabelId, bottomLabelId) {
    var dataWidth = bGraphUtils.getXFromDayNumber(daysCount) + barWidth;
    graph      = Raphael(graphId,      dataWidth, linesGraphHeight);
    bgGraph    = Raphael(bgGraphId,    99999, 250);
    rightLabel = Raphael(rightLabelId, gw, 325);
    leftLabel  = Raphael(leftLabelId,  gw, 325);
    bottomLabel= Raphael(bottomLabelId,  gw, 75);

    return [ graph, bgGraph, rightLabel, leftLabel, bottomLabel];
  }

  function prepareLinesGraphData(jsonData) {
    daysCount = 0;
    for (var i = 0; i < jsonData.length; i++) {
      daysCount += jsonData[i].days.length;
    }
    var dataWidth = bGraphUtils.getXFromDayNumber(daysCount) + barWidth;
    linesGraphVw  = Math.max(linesGraphVw, dataWidth);
  }

  function getNormalizedHeight(value){
    return value/maxValue * linesGraphVh;
  }

});