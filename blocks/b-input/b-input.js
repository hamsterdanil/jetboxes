var bInputs = {
	maskPhoneInputs: function() {
		$(".js-input_mask_phone").mask("+7 (999) 999-99-99",{placeholder:"*"});
	},
	unmaskInput: function(input) {
		input.unmask();
	}
}
bInputs.maskPhoneInputs();