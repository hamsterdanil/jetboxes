var bComments = {

	addComment: function(commentData) {
		var newComment = tmpl("b_comments_template", commentData);
		$(".b-comments-list").append(newComment);
		$(".b-comments-list").find(".b-comment_fresh").slideDown(300, function() {
			$(this).removeClass("b-comment_fresh");
		});
	},
	showAll: function(commentsWrapper) {
		commentsWrapper.find(".b-comments__showall").slideUp();
		commentsWrapper.find(".b-comment_hidden").slideDown(300, function() {
			$(this).removeClass("b-comment_hidden");
		});
	}
};


$(document).on("click", ".b-comments__showall", function() {
	var self = $(this);
	var commentsWrapper = self.closest(".b-comments");

	bComments.showAll(commentsWrapper);

	return false;
});


