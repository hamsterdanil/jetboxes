/* global tmpl */

jQuery.validator.setDefaults({
	errorClass: "b-form__error b-form__error_indent"
});

$("#js-form_login_cabinet").validate({
	errorClass: "b-form__error",
	rules: {
		userpass: {
			required: true,
			minlength: 4
		},
		useremail: {
			required: true,
			email: true
		}
	},
	messages: {
		useremail: {
			required: "Введите адрес электронной почты",
			email: "Неверный адрес электронной почты"
		},
		userpass: {
			required: "Неверный пароль",
			minlength: "Длина пароля должна быть больше 4 символов"
		},
	}
});

$("#js-form_password_change").validate({
	rules: {
		emailrecovery: {
			required: true,
			email: true
		}
	},
	messages: {
		emailrecovery: {
			required: "Введите адрес электронной почты",
			email: "Неверный адрес электронной почты"
		}
	},
	submitHandler: function(form) {
		if ($(form).valid()) {
			window.location.href = "sendmail.html";
			// form.submit();
		} else {
			// return false;
		}
	}
});

jQuery.validator.addMethod("isNewUserLogin", function() {
	return $("#user_email").val() !== $("#user_email_new").val();
}, "* Must be new value");

var formOptionsLogin = $("#js-form_options_login").validate({
	onfocusout: function () {
		formOptionsLogin.resetForm();
	},
	errorClass: "b-form__error",
	rules: {
		user_email_new: {
			required: true,
			email: true,
			isNewUserLogin: true
		}
	},
	messages: {
		user_email_new: {
			required: "Введите новый адрес",
			email:    "Введите корректный адрес электронной почты",
			isNewUserLogin: "Вы ввели текущий адрес"
		}
	},
	submitHandler: function(form) {
		if ($(form).valid()) {
			console.log($(form).valid());
		} else {
			return false;
		}
	}
});

jQuery.validator.addMethod("isNewPasswordEntered", function() {
	return $("#old_password").val() !== $("#new_password").val();
}, "* Must enter new different password");
jQuery.validator.addMethod("isNewPasswordAgainTheSame", function() {
	return $("#new_password").val() === $("#new_password_again").val();
}, "* Must repeat same password");

var formOptionsPassword = $("#js-form_options_password").validate({
	onfocusout: function () {
		formOptionsPassword.resetForm();
	},
	errorClass: "b-form__error",
	rules: {
		new_password: {
			required: true,
			minlength: 4,
			isNewPasswordEntered: true
		},
		new_password_again: {
			required: true,
			minlength: 4,
			isNewPasswordAgainTheSame: true
		}
	},
	messages: {
		new_password: {
			required: "Введите новый пароль",
			isNewPasswordEntered: "Вы ввели старый пароль",
			minlength: "Длина пароля должна быть больше 4 символов"
		},
		new_password_again: {
			required: "Повторите новый пароль",
			isNewPasswordAgainTheSame: "Вы ввели пароль, отличный от ранее введенного",
			minlength: "Длина пароля должна быть больше 4 символов"
		}
	},
	submitHandler: function(form) {
		if ($(form).valid()) {
			console.log($(form).valid());
		} else {
			return false;
		}
	}
});


var formOptionsAddPeople = $("#js-form_options_adduser").validate({
	errorClass: "b-form__error",
	rules: {
		people_name: { required: true },
		options_adduser_day: { required: true },
		options_adduser_month: { required: true },
		options_adduser_year: { required: true }
	},
	messages: {
		people_name: {
			required: "Введите имя"
		},
		"choose-sex": "Выберите пол",
		options_adduser_day: "Выберите дату рождения",
		options_adduser_month: "Выберите дату рождения",
		options_adduser_year: "Выберите дату рождения"
	},
	groups: {
		dateOfBirth: "options_adduser_day options_adduser_month options_adduser_year"
	},
	errorPlacement: function(error, element) {
		if (element.attr("name") === "options_adduser_day" || element.attr("name") === "options_adduser_month" || element.attr("name") === "options_adduser_year") {
			error.insertAfter("#b-select_options_adduser_year");
		} else if (element.hasClass("b-customInput")) {
			var lastLabel = element.closest(".b-form__fieldset").find(".b-customInput__label").last();
			error.insertAfter(lastLabel);
		} else {
			error.insertAfter(element);
		}
	},
	submitHandler: function(form) {
		if ($(form).valid()) {
			// form.submit();

			var day   = $("#options_adduser_day").val();
			if (day < 10) {
				day = "0"+day;
			}
			var month = $("#options_adduser_month").val();
			if (month < 10) {
				month = "0"+month;
			}
			var year  = $("#options_adduser_year").val();
			var birthday = day+"."+month+"."+year;
			var sex  = $("input[name='choose-sex']:checked").val();
			
			var data = {
				name: $("#people_name").val(),
				birthday: birthday,
				sex: sex === "male" ? "Мужской" : "Женский",
			};
			$("#b-form-adduser__list").append(tmpl("adduser_template", data));
			form.reset();
			$(form).find(".b-select").dropkick("reset");

			return false;
		} else {
			return false;
		}
	}
});

var formOptionsYourName = $("#js-form_options_yourname").validate({
	onfocusout: function () {
		formOptionsYourName.resetForm();
	},
	errorClass: "b-form__error",
	rules: {
		user_name_edit: {
			required: true,
			minlength: 4
		}
	},
	messages: {
		user_name_edit: {
			required:  "",
			minlength: ""
		}
	},
	submitHandler: function(form) {
		if ($(form).valid()) {
			// form.submit();
			// ajax
			var nameInput = $("#user_name");
			nameInput.val( $("#user_name_edit").val() );
			$(form).trigger("setFormModeView", $(form));

			return false;
		} else {
			return false;
		}
	}
});

