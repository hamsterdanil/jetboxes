$("#b-popup_add-user").validate({
	errorClass: "b-form__error",
	// messages: ,
	errorPlacement: function(error, element) {
		if (element.hasClass("b-select")) {
			var select = element.next("div.b-select");
			error.insertAfter(select);
		} else if (element.hasClass("b-customInput")) {
			var lastLabel = element.closest(".b-form__fieldset").find(".b-customInput__label").last();
			error.insertAfter(lastLabel);
		} else {
			error.insertAfter(element);
		}
	},
	submitHandler: function(form) {
		// if ($(form).valid()) {

		// }
		return false;
	}
});