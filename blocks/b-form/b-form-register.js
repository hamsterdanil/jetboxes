function checkNewNumberInputs() {
	var phonesCount = $("#b-form-addnumbers__list").find(".l-layout__item_phonenumbers").length;
	if (phonesCount >2){
		$("#js-form_addnewnumber").hide();
		$(".js-form_addnewnumber_submit").off().hide();
	} else {
		$("#js-form_addnewnumber").show();
		$(".js-form_addnewnumber_submit").show();
	}
}

function testNewNumberValidityNoForm() {
	bInputs.unmaskInput( $("#my_new_phone") );
	var isValid = $("#my_new_phone").val().length > 1;
	bInputs.maskPhoneInputs();
	return isValid;
}

$(".js-form_addnewnumber_submit").on("click", function(e) {
	if (testNewNumberValidityNoForm()) {
		var phone  = $("#my_new_phone").val();
		var data = {
			phone: phone
		};
		$("#b-form-addnumbers__list").append(tmpl("addnumber_register_template", data));
		$("#my_new_phone").val("").removeClass("b-form__error")
			.next("label.b-form__error").hide();
	}
	$("#my_new_phone").focus();
	checkNewNumberInputs();
	return false;
});


$("#js-popup__registration input[name='amount']").on("change", function() {
	var selectedAmount = parseInt( $('input[name="amount"]:checked').val(), 10);
	var existingAmount = $("#b-form-addusers__list .b-content__selection").length;

	if (selectedAmount > existingAmount){
		for (var i = existingAmount; i < selectedAmount; i++) {
			var data = {
				user_number: i+1
			};
			$("#b-form-addusers__list").append(tmpl("adduser_template", data));
			bCustomInputs.init();
			$("#b-form-addusers__list .b-select").dropkick();
		}
	} else if (selectedAmount < existingAmount){
		$("#b-form-addusers__list .b-content__selection")
			.slice(selectedAmount).remove();
	} else {
		return;
	}
});


// validation for up to 7 forms (/register page)
var maxPeopleCount = 7;
var dynGroups = {};
var dynMessages = {
	"registration-name": {
		required: "Введите адрес электронной почты",
		email: "Неверный адрес электронной почты"
	},
	"registration-pwd": {
		required: "Введите пароль",
		minlength: "Длина пароля должна быть больше 4 символов"
	},
	my_new_phone: {
		required: "Введите номер телефона"
	}
};
for (var i = 1; i <= maxPeopleCount; i++) {
	dynMessages["day-" + i]        = { required: "Введите дату рождения" };
	dynMessages["month-" + i]      = { required: "Введите дату рождения" };
	dynMessages["year-" + i]       = { required: "Введите дату рождения" };
	dynMessages["name-" + i]       = { required: "Введите имя" };
	dynMessages["choose-sex-" + i] = { required: "Укажите пол" };
	dynGroups["dudeBirthday" + i]  = "day-"+i+" month-"+i+" year-"+i;
}


jQuery.validator.addMethod("atLeastOnePhoneAdded", function(value, element) {
	return this.optional(element) || $("#b-form-addnumbers__list").find(".l-layout__item_phonenumbers").length;
}, "Нужно добавить минимум один номер телефона");

var popupRegistration = $("#js-popup__registration").validate({
	errorClass: "b-form__error",
	groups: dynGroups,
	messages: dynMessages,
	errorPlacement: function(error, element) {
		if (element.hasClass("b-select")) {
			var select = element.next("div.b-select");
			error.insertAfter(select);
		} else if (element.hasClass("b-customInput")) {
			var lastLabel = element.closest(".b-form__fieldset").find(".b-customInput__label").last();
			error.insertAfter(lastLabel);
		} else {
			error.insertAfter(element);
		}
	},
	rules: {
		my_new_phone: { atLeastOnePhoneAdded : true }
	},
	submitHandler: function(form) {
		// if ($(form).valid()) {

		// }
		return false;
	}
});


$(document).on("click", ".js-form__savenumber-btn", function() {
	var self = $(this);
	var wrapper = self.closest(".l-layout__item_phonenumbers");
	var phoneInput = wrapper.find(".js-form_addnumbers_phone_input");

	bInputs.unmaskInput( phoneInput );
	var isValid = phoneInput.val().length > 1;
	if (isValid) {

		// form.submit();
		var newNumberLabel = wrapper.find(".js-form_addnumbers_newphone");
		var newValue  = phoneInput.val();

		// ajax

		newNumberLabel.text( newValue );
		wrapper.trigger("setFormModeView", wrapper);
	} else {
		console.log("not valid");
	}
	bInputs.maskPhoneInputs();

	return false;
});

$(document).on("click", ".js-form__delete-item", function() {
	// TODO
	alert("Вы действительно хотите удалить? попап");
	var self = $(this);
	var form = self.closest(".js-form");
	
	// ajax
	form.remove();
	checkNewNumberInputs();
	return false;
});
