var validator = $("#b-comment__add-area").validate({
	onfocusout: function () {
		validator.resetForm();
	},
	messages: {
		comment_text: {
			required: ""
		}
	},
	errorClass: "b-form__error",
	submitHandler: function(form) {
		if ($(form).valid()) {
			// form.submit();
			// ajax

			var input = $("input[name='comment_text']");
			var commentTxt = input.val();
			input.val("");
			var commentData = {
				author: "Пупкин",
				text: commentTxt,
				date: moment().format("D MMMM YYYY HH:mm")
			};
			bComments.addComment(commentData);
		} else {
			console.log("not valid");
		}
		return false;
	}
});


