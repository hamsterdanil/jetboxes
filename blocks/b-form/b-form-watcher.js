/* global tmpl */
function addNewWatcherValidation() {
	var newWatcherForms = $(".js-form_options_addwatcher");
	newWatcherForms.each(function(index, form) {
		var validator = $(form).validate({
			onfocusout: function () {
				validator.resetForm();
			},
			errorClass: "b-form__error",
			submitHandler: function(form) {
				console.log($(form), '$(form)');
				var phoneLabel = $(form).find(".js-form_options_addwatcher_newemail");
				var newPhoneValue  = $(form).find(".js-form_options_addwatcher_phone_input").val();

				if ($(form).valid()) {
					// form.submit();
					// ajax
					phoneLabel.text( newPhoneValue );
					$(form).trigger("setFormModeView", $(form));
				} else {
					console.log("no");
				}
				return false;
			}
		});
		var input = $(form).find(".js-form_options_addwatcher_phone_input");
		if (input.length){
			input.rules("add", {
				messages: {
					required: "",
					email: ""
				}
			});
		}
	});
}
addNewWatcherValidation();

var formOptionsAddWatcher = $("#js-form_options_addnewwatcher").validate({
	onfocusout: function () {
		formOptionsAddWatcher.resetForm();
	},
	errorClass: "b-form__error",
	rules: {
		new_watcher_email: {
			required: true,
			email: true
		}
	},
	messages: {
		new_watcher_email: {
			required: "",
			email:    ""
		}
	},
	submitHandler: function(form) {
		if ($(form).valid()) {
			// form.submit();
		} else {
			console.log("no");
		}
		return false;
	}
});

$(".js-form_options_addnewwatcher_submit").on("click", function() {
	if ($("#js-form_options_addnewwatcher").valid()) {
		var email  = $("#new_watcher_email").val();
		var data = {
			email: email
		};
		$("#b-form-addwatchers__list").append(tmpl("addwatcher_template", data));
		addNewWatcherValidation();
		$("#js-form_options_addnewwatcher")[0].reset();
	} else {
		$("#new_watcher_email").focus();
	}
	return false;
});