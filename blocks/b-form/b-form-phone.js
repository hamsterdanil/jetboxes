/* global tmpl, bInputs */
function addNewNumberValidation() {
	var newNumberForms = $(".js-form_options_addnumbers");
	newNumberForms.each(function(index, form) {
		var validator = $(form).validate({
			onfocusout: function () {
				validator.resetForm();
			},
			errorClass: "b-form__error",
			submitHandler: function(form) {
				bInputs.unmaskInput($(form).find(".js-input_mask_phone"));
				var newNumberLabel = $(form).find(".js-form_options_addnumbers_newphone");
				var newValue  = $(form).find(".js-form_options_addnumbers_phone_input").val();

				if ($(form).valid()) {
					// form.submit();
					// ajax
					newNumberLabel.text( newValue );
					$(form).trigger("setFormModeView", $(form));
				} else {
					console.log("not valid");
				}
				return false;
			}
		});
		var input = $(form).find(".js-form_options_addnumbers_phone_input");
		if (input.length){
			input.rules("add", {
				messages: {
					required: ""
				}
			});
		}
	});
}
addNewNumberValidation();


$(".js-form_options_addnewnumber_submit").on("click", function() {
	if ($("#js-form_options_addnewnumber").valid()) {
		var phone  = $("#my_new_phone").val();
		var data = {
			phone: phone
		};
		$("#b-form-addnumbers__list").append(tmpl("addnumber_template", data));
		addNewNumberValidation();
		$("#js-form_options_addnewnumber")[0].reset();
	} else {
		$("#my_new_phone").focus();
	}
	return false;
});
