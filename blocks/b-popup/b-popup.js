(function(){
	'use strict';

	var popupShowEvent = 'bPopupShowEvent';

	var bPopup = {
		init: function() {
			$('.js-popup__close').on('click', function(e) {
				var self = $(this).closest('.b-popup');
				bPopup.close(self);

				e.preventDefault();
			});
			$(document).on('click', '[data-popup-open]', function(e) {
				$(this).closest('.b-popup').hide();
				var popupGoToSelector = $(this).data('popup-open');
				// console.log(popupGoToSelector);
				bPopup.show( $(popupGoToSelector) );

				e.preventDefault();
			});
			$(document).on('click', '[data-popup-valid-open]', function(e) {
				var link = $(this);
				var form = link.closest('form');
				var popup = link.closest('.b-popup');

				if (form.valid()){
					bPopup.close(popup);
					var popupGoToSelector = link.data('popup-valid-open');
					bPopup.show( $(popupGoToSelector) );
				}
				e.preventDefault();
			});

			$(document).on('click', '.b-popup_datapicker', function(e) {
				return false;
			});
			$(document).on('click', '.js-popup_datapicker_opener', function(e) {
				var link = $(this);
				var wrapper = link.closest('.js-popup_datapicker-wrapper');
				bPopup.show( wrapper.find('.js-popup_datapicker') );
				return false;
			});
			$(document).on('click', function() {
				bPopup.close( $('.js-popup_datapicker') );
			});
		},

		show: function(popup) {
			popup.show();
			$(document).trigger(popupShowEvent);
		},
		close: function(popup) {
			popup.hide();
		},

		closePopupAfterTimeout: function() {
			var popupToAutoClose  = $('.b-popup:visible[data-popup-close-after]');
			var time              = popupToAutoClose.data('popup-close-after');
			var popupOpenSelector = popupToAutoClose.data('popup-open');
			if(time){
				setTimeout(function() {
					bPopup.close( popupToAutoClose );
					bPopup.show( $(popupOpenSelector) );
				}, time);
			}
		}
	};

	$(document)
		.ready( bPopup.init )
		.on(popupShowEvent, bPopup.closePopupAfterTimeout);


	$('.js-popup_add-user').magnificPopup({
		items: {
			src: '#b_popup_add_user',
			type: 'inline'
		}
	});
	$('.js-popup_add-user').on('mfpOpen', function(e /*, params */) {

		$('#b_popup_add_user').validate({
			errorClass: 'b-form__error',
			groups: {
				'dudeBirthday': 'adduser_day adduser_month adduser_year'
			},
			messages: {
				my_new_phone:  'Введите номер телефона',
				adduser_day:   'Введите дату рождения',
				adduser_month: 'Введите дату рождения',
				adduser_year:  'Введите дату рождения',
				adduser_name:  'Введите имя',
				'adduser_choose-sex': 'Укажите пол'
			},
			errorPlacement: function(error, element) {
				if (element.hasClass('b-select')) {
					var select = element.next('div.b-select');
					error.insertAfter(select);
				} else if (element.hasClass('b-customInput')) {
					var lastLabel = element.closest('.b-form__fieldset').find('.b-customInput__label').last();
					error.insertAfter(lastLabel);
				} else {
					error.insertAfter(element);
				}
			},
			submitHandler: function(form) {
				if ( $('#b_popup_add_user').valid() ) {
					bTabs.addTab( $('input[name="adduser_name"]').val() );
					$.magnificPopup.close();
					$('#b_popup_add_user')[0].reset();
				}
				return false;
			}
		});

	});

})();