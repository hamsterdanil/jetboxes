// moment.js language configuration
// language : russian (ru)
// author : Viktorminator : https://github.com/Viktorminator
// Author : Menelion Elensúle : https://github.com/Oire

function plural(word, num) {
    var forms = word.split('_');
    return num % 10 === 1 && num % 100 !== 11 ? forms[0] : (num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20) ? forms[1] : forms[2]);
}

function relativeTimeWithPlural(number, withoutSuffix, key) {
    var format = {
        'mm': 'минута_минуты_минут',
        'hh': 'час_часа_часов',
        'dd': 'день_дня_дней',
        'MM': 'месяц_месяца_месяцев',
        'yy': 'год_года_лет'
    };
    if (key === 'm') {
        return withoutSuffix ? 'минута' : 'минуту';
    }
    else {
        return number + ' ' + plural(format[key], +number);
    }
}

function monthsCaseReplace(m, format) {
    var months = {
        'nominative': 'январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь'.split('_'),
        'accusative': 'января_февраля_марта_апреля_мая_июня_июля_августа_сентября_октября_ноября_декабря'.split('_')
    },

    nounCase = (/D[oD]? *MMMM?/).test(format) ?
        'accusative' :
        'nominative';

    return months[nounCase][m.month()];
}

function weekdaysCaseReplace(m, format) {
    var weekdays = {
        'nominative': 'воскресенье_понедельник_вторник_среда_четверг_пятница_суббота'.split('_'),
        'accusative': 'воскресенье_понедельник_вторник_среду_четверг_пятницу_субботу'.split('_')
    },

    nounCase = (/\[ ?[Вв] ?(?:прошлую|следующую)? ?\] ?dddd/).test(format) ?
        'accusative' :
        'nominative';

    return weekdays[nounCase][m.day()];
}

moment.lang('ru', {
    months : monthsCaseReplace,
    monthsShort : "янв_фев_мар_апр_май_июн_июл_авг_сен_окт_ноя_дек".split("_"),
    weekdays : weekdaysCaseReplace,
    weekdaysShort : "вск_пнд_втр_срд_чтв_птн_сбт".split("_"),
    weekdaysMin : "вс_пн_вт_ср_чт_пт_сб".split("_"),
    longDateFormat : {
        LT : "HH:mm",
        L : "DD.MM.YYYY",
        LL : "D MMMM YYYY г.",
        LLL : "D MMMM YYYY г., LT",
        LLLL : "dddd, D MMMM YYYY г., LT"
    },
    calendar : {
        sameDay: '[Сегодня в] LT',
        nextDay: '[Завтра в] LT',
        lastDay: '[Вчера в] LT',
        nextWeek: function () {
            return this.day() === 2 ? '[Во] dddd [в] LT' : '[В] dddd [в] LT';
        },
        lastWeek: function () {
            switch (this.day()) {
            case 0:
                return '[В прошлое] dddd [в] LT';
            case 1:
            case 2:
            case 4:
                return '[В прошлый] dddd [в] LT';
            case 3:
            case 5:
            case 6:
                return '[В прошлую] dddd [в] LT';
            }
        },
        sameElse: 'L'
    },
    relativeTime : {
        future : "через %s",
        past : "%s назад",
        s : "несколько секунд",
        m : relativeTimeWithPlural,
        mm : relativeTimeWithPlural,
        h : "час",
        hh : relativeTimeWithPlural,
        d : "день",
        dd : relativeTimeWithPlural,
        M : "месяц",
        MM : relativeTimeWithPlural,
        y : "год",
        yy : relativeTimeWithPlural
    },

    ordinal: function (number, period) {
        switch (period) {
        case 'M':
        case 'd':
        case 'DDD':
            return number + '-й';
        case 'D':
            return number + '-го';
        case 'w':
        case 'W':
            return number + '-я';
        default:
            return number;
        }
    },

    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 7  // The week that contains Jan 1st is the first week of the year.
    }
});







var datepicker1 = document.getElementById("datepicker-startdate");
var datepicker2 = document.getElementById("datepicker-enddate");

if(datepicker1 && datepicker2){
    var now = moment();

    var oneWeekEarlier    = moment(now).subtract("week", 1);
    var twoWeeksEarlier   = moment(now).subtract("week", 2);
    var oneMonthEarlier   = moment(now).subtract("month", 1);
    var threeMonthEarlier = moment(now).subtract("month", 3);
    var halfYearEarlier   = moment(now).subtract("month", 6);
    var yearEarlier       = moment(now).subtract("year", 1);

    var startOfTime       = moment(now).subtract('year', 2);


    // datepicker2.value = now.format("D MMMM YYYY");

    function setStartDate(self, moment) {
        var datepicker = $(self).closest(".js-popup_datapicker").find(".js-input_datepicker_start");
        datepicker[0].value = moment.format("D MMMM YYYY");
        $("#label-startdate").text( moment.format("D MMMM") );
    }
    function setEndDate(self, moment) {
        var datepicker = $(self).closest(".js-popup_datapicker").find(".js-input_datepicker_end");
        datepicker[0].value = moment.format("D MMMM YYYY");
        $("#label-enddate").text( moment.format("D MMMM YYYY") );
    }

    $('.js-popup_datapicker-1week').click(function() {
        setStartDate( this, oneWeekEarlier);
    });
    $('.js-popup_datapicker-2week').click(function() {
        setStartDate( this, twoWeeksEarlier);
    });
    $('.js-popup_datapicker-1month').click(function() {
        setStartDate( this, oneMonthEarlier);
    });
    $('.js-popup_datapicker-3month').click(function() {
        setStartDate( this, threeMonthEarlier);
    });
    $('.js-popup_datapicker-6month').click(function() {
        setStartDate( this, halfYearEarlier);
    });
    $('.js-popup_datapicker-1year').click(function() {
        setStartDate( this, yearEarlier);
    });
    $('.js-popup_datapicker-all').click(function() {
        setStartDate( this, startOfTime);
    });

    // console.log(now.format("D MMMM YYYY"), 'now.format("D MMMM YYYY")');
    // console.log(oneWeekEarlier.format("D MMMM YYYY"), 'oneWeekEarlier.format("D MMMM YYYY")');
    // console.log(twoWeeksEarlier.format("D MMMM YYYY"), 'twoWeeksEarlier.format("D MMMM YYYY")');
    // console.log(oneMonthEarlier.format("D MMMM YYYY"), 'oneMonthEarlier.format("D MMMM YYYY")');
    // console.log(threeMonthEarlier.format("D MMMM YYYY"), 'threeMonthEarlier.format("D MMMM YYYY")');
    // console.log(halfYearEarlier.format("D MMMM YYYY"), 'halfYearEarlier.format("D MMMM YYYY")');
    // console.log(yearEarlier.format("D MMMM YYYY"), 'yearEarlier.format("D MMMM YYYY")');

    new Pikaday({
      field: datepicker1,
      i18n: {
        previousMonth : "Перд Месяц",
        nextMonth     : "След Месяц",
        months        : ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],
        weekdays      : ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],
        weekdaysShort : ["Вс","Пн","Вт","Ср","Чт","Пт","Сб"]
      },
      firstDay: 1,
      format: 'D MMMM YYYY'
    });

    new Pikaday({
      field: datepicker2,
      i18n: {
        previousMonth : "Перд Месяц",
        nextMonth     : "След Месяц",
        months        : ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],
        weekdays      : ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],
        weekdaysShort : ["Вс","Пн","Вт","Ср","Чт","Пт","Сб"]
      },
      firstDay: 1,
      format: "D MMMM YYYY"
    });
}

var datepickerIds = [
    "datepicker-startdate-health",
    "datepicker-enddate-health",
    "datepicker-startdate-safety",
    "datepicker-enddate-safety"
];
var datepickers = [];

for (var i = 0; i < datepickerIds.length; i++) {
    datepickers.push(document.getElementById(datepickerIds[i]));
}

for (var i = 0; i < datepickers.length; i++) {
    (function(i) {
        if (datepickers[i]){
            new Pikaday({
              field: datepickers[i],
              i18n: {
                previousMonth : "Перд Месяц",
                nextMonth     : "След Месяц",
                months        : ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],
                weekdays      : ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],
                weekdaysShort : ["Вс","Пн","Вт","Ср","Чт","Пт","Сб"]
              },
              firstDay: 1,
              format: "D MMMM YYYY"
            });
        }
    })(i);
    (function(j) {
        return function(i) {
            
        };
    })(i);
};






